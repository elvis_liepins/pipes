import React from 'react'
import './App.css'
import PipesPage from 'pages/pipes'
import { createUseStyles } from 'react-jss'

const useStyles = createUseStyles({
  container: {
    display: 'flex',
    justifyContent: 'center',
    height: '100%',
  },
})

function App() {
  const classes = useStyles()
  return (
    <div className={classes.container}>
      <PipesPage />
    </div>
  )
}

export default App
