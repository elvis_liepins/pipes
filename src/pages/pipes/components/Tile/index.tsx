import React, { useContext, useState } from 'react'
import { useDispatch } from 'react-redux'
import { IconButton } from '@material-ui/core'
import { createUseStyles } from 'react-jss'

import { SocketContext } from 'pages/pipes/context/socket'
import { changeTile } from 'reducers/map'
import { PipeTiles } from '../../types'
import { mappedIcons, mappedInfo } from '../../utils/'
import { TileProps } from 'components/TileMap/types'

type RuleNames = 'icon'

const useStyles = createUseStyles<RuleNames, number>({
  icon: {
    transform: (rot) => `rotate(${rot * 90}deg)`,
    overflow: 'hidden',
    transitionDuration: '0.5s',
    transitionProperty: 'transform',
  },
})

export const Tile = ({ data, cords }: TileProps) => {
  const [rotatedSymbol, setSymbol] = useState(data as PipeTiles)
  const info = mappedInfo[rotatedSymbol]
  const [rot, setRot] = useState(info.rotation as number)
  const classes = useStyles(rot)

  const { send } = useContext(SocketContext)
  const dispatch = useDispatch()

  const handleClick = () => {
    const command = `rotate ${cords.x} ${cords.y}`
    send(command)
    dispatch(changeTile({ cords, info: info.next }))
    setRot(rot + 1)
    setSymbol(info.next)
  }

  const Icon = mappedIcons[info.type]
  return (
    <IconButton
      style={{
        padding: '1px',
      }}
      onClick={handleClick}
    >
      <Icon className={classes.icon} />
    </IconButton>
  )
}
