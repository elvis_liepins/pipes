import React, { useContext } from 'react'
import { SocketContext } from 'pages/pipes/context/socket'
import { Button } from '@material-ui/core'
import { useDispatch, useSelector } from 'react-redux'
import { changeMap, setMap } from 'reducers/map'
import { Solver } from 'pages/pipes/utils/Solver'
import { RootState } from 'reducers/store'
import { createUseStyles } from 'react-jss'
import { Level, SceneContext } from 'pages/pipes/context/scene'

const useStyles = createUseStyles({
  container: {
    paddingTop: '20px',
    display: 'flex',
    justifyContent: 'center',
    '& > button': {
      marginLeft: '5px',
    },
  },
})

export const Controls = () => {
  const { client, send } = useContext(SocketContext)
  const { level, setScene } = useContext(SceneContext)
  const { data, height, length } = useSelector((state: RootState) => state.map)
  const dispatch = useDispatch()
  const classes = useStyles()

  const handleSolve = async () => {
    const solver = new Solver(data, height, length)
    solver.processAsync(data, client).then((res) => dispatch(changeMap(res)))
  }

  return (
    <div className={classes.container}>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          setScene('game', level)
          send(`new ${level}`)
          send('map')
        }}
      >
        new
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={() => send('verify')}
      >
        verify
      </Button>
      <Button variant="contained" color="primary" onClick={handleSolve}>
        solve
      </Button>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          dispatch(setMap([]))
          setScene('menu', 1 as Level)
        }}
      >
        back
      </Button>
    </div>
  )
}
