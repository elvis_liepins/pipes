import React, { useContext } from 'react'
import { SocketContext } from 'pages/pipes/context/socket'
import { createUseStyles } from 'react-jss'
import { Button } from '@material-ui/core'
import { Level, SceneContext } from 'pages/pipes/context/scene'

const useStyles = createUseStyles({
  container: {
    display: 'flex',
    justifyContent: 'center',
    '& > button': {
      marginLeft: '5px',
    },
  },
})

export const LevelButtons = () => {
  const classes = useStyles()
  const { send } = useContext(SocketContext)
  const { setScene } = useContext(SceneContext)
  const handleClick = (index: number) => {
    send(`new ${index + 1}`)
    send('map')
    setScene('game', (index + 1) as Level)
  }

  return (
    <div className={classes.container}>
      {[...Array(6)].map((v, i) => (
        <Button
          key={`levelButton-${i}`}
          onClick={() => handleClick(i)}
          variant="contained"
          color="secondary"
        >
          {i + 1}
        </Button>
      ))}
    </div>
  )
}
