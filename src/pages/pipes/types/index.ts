// export type TTile = typeof PipeSymbols.TUp | typeof PipeSymbols.TRight | typeof PipeSymbols.TLeft | typeof PipeSymbols.TDown;
// export type LineTile = typeof PipeSymbols.Horizontal | typeof PipeSymbols.Vertical;
// export type HalfTile = typeof PipeSymbols.HalfUp | typeof PipeSymbols.HalfRight | typeof PipeSymbols.HalfDown | typeof PipeSymbols.HalfLeft;
// export type Cross = typeof PipeSymbols.Cross;
// export type Tiles = TTile | LineTile | HalfTile | Cross;

export enum PipeSymbols {
  TUp = '┻',
  TRight = '┣',
  TDown = '┳',
  TLeft = '┫',
  HalfUp = '╹',
  HalfRight = '╺',
  HalfDown = '╻',
  HalfLeft = '╸',
  Horizontal = '━',
  Vertical = '┃',
  Cross = '╋',
  LUp = '┗',
  LRight = '┏',
  LDown = '┓',
  LLeft = '┛',
}

export enum PipeTiles {
  TUp,
  TRight,
  TDown,
  TLeft,
  HalfUp,
  HalfRight,
  HalfDown,
  HalfLeft,
  Horizontal,
  Vertical,
  Cross,
  LUp,
  LRight,
  LDown,
  LLeft,
}

export enum PipeShapes {
  Cross,
  Half,
  T,
  Line,
  L,
}

export enum Sides {
  up = 'up',
  right = 'right',
  down = 'down',
  left = 'left',
}

export interface Connections {
  [Sides.up]?: boolean
  [Sides.right]?: boolean
  [Sides.down]?: boolean
  [Sides.left]?: boolean
}

export interface TileInfo {
  symbol: PipeTiles
  type: PipeShapes
  rotation: 0 | 1 | 2 | 3
  next: PipeTiles
  connects: Connections
}

export interface LegalRotations {
  0?: boolean
  1?: boolean
  2?: boolean
  3?: boolean
}
