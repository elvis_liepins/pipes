import { Connections, PipeShapes, Sides, TileInfo } from 'pages/pipes/types'
import { Pos } from 'types'
import { defConnections, mappedSymbols } from '..'
import { Solver } from '.'

export function shouldSkipHalf(this: Solver, pos: Pos): boolean {
  const otherInfo = this.getInfo(this.data.get(pos.y, pos.x))
  return otherInfo.type !== PipeShapes.Half || !!this.solved[pos.y][pos.x]
}

export function processHalf(this: Solver, pos: Pos, side: Sides): boolean {
  const otherInfo = this.getInfo(this.data.get(pos.y, pos.x))
  if (otherInfo.type !== PipeShapes.Half || !!this.solved[pos.y][pos.x])
    return false
  return this.filterRotations({ x: pos.x, y: pos.y }, side, false, 1)
}

export function doUnsolved(this: Solver, pos: Pos) {
  const symbol = this.data.get(pos.y, pos.x)
  const info = this.getInfo(symbol)

  switch (info.type) {
    case PipeShapes.Cross:
      this.solved[pos.y][pos.x] = 1

      this.legalRotations.set(pos.y, pos.x, 0, 1)
      this.legalRotations.set(pos.y, pos.x, 1, 0)
      this.legalRotations.set(pos.y, pos.x, 2, 0)
      this.legalRotations.set(pos.y, pos.x, 3, 0)

      this.queue.push({ x: pos.x, y: pos.y })
      break
    case PipeShapes.Line: {
      this.legalRotations.set(pos.y, pos.x, 2, 0)
      this.legalRotations.set(pos.y, pos.x, 3, 0)

      if (this.getRotationNumber(pos) === 1) {
        this.solved[pos.y][pos.x] = 1
        this.queue.push({ x: pos.x, y: pos.y })
      }
      break
    }
    case PipeShapes.Half:
      //half pipes should not connect to other half pipes
      this.loopSides(
        pos,
        (otherPos, side) =>
          this.filterRotations({ x: pos.x, y: pos.y }, side, false),
        this.shouldSkipHalf,
      )
      if (this.getRotationNumber(pos) > 2) this.doIntersections(pos, symbol)
      break
    case PipeShapes.T:
      if (this.getRotationNumber(pos) < 4) this.doIntersections(pos, symbol)
      break
    case PipeShapes.L:
      if (this.getRotationNumber(pos) < 3) this.doIntersections(pos, symbol)
      break
  }
}

export function doIntersections(this: Solver, pos: Pos, symbol: number) {
  const symbols = mappedSymbols[this.getInfo(symbol).type]
  const isValid = (info: TileInfo) =>
    this.legalRotations.get(pos.y, pos.x, info.rotation) === 1
  const legalSymbolInfo = symbols.map(this.getInfo).filter(isValid)

  const connections: Connections = { ...defConnections }
  const sides: Connections = { ...defConnections }

  // get intersection of available connections and intersection of no connections
  const reduceFn = (
    [conns, sides]: [Connections, Connections],
    info: TileInfo,
  ): [Connections, Connections] => {
    Object.keys(conns).forEach(
      (conn) => !info.connects[conn as Sides] && delete conns[conn as Sides],
    )
    Object.keys(sides).forEach(
      (side) => info.connects[side as Sides] && delete sides[side as Sides],
    )
    return [conns, sides]
  }

  legalSymbolInfo.reduce(reduceFn, [connections, sides])
  this.doSolved(pos, 1, connections, sides)
}
