import ndarray from 'ndarray'
import { Sides } from 'pages/pipes/types'
import { Pos } from 'types'
import { Solver } from '.'

export function doSides(this: Solver) {
  for (let x = 0; x < this.length; x++) {
    //horizontal
    this.filterRotations({ x, y: 0 }, Sides.up, false, 1)
    this.filterRotations({ x, y: this.height - 1 }, Sides.down, false, 1)
  }
  for (let y = 0; y < this.height; y++) {
    // vertical
    this.filterRotations({ x: 0, y }, Sides.left, false, 1)
    this.filterRotations({ x: this.length - 1, y }, Sides.right, false, 1)
  }
}

export function saveValues(this: Solver) {
  this.savedRotations = ndarray(Uint8Array.from(this.legalRotations.data), [
    this.height,
    this.length,
    4,
  ])
  this.savedSolved = this.solved.map((row) => [...row])
  this.savedGroupMap = this.groupMap.map((row) => [...row])
}

export function generateGroupMap(this: Solver) {
  let groupNum = 1
  for (let y = 0; y < this.height; y++) {
    for (let x = 0; x < this.length; x++) {
      if (this.solved[y][x] && !this.groupMap[y][x]) {
        const key = groupNum
        const count = this.makeGroupMap({ x, y }, key)
        this.groupSizes[key] = count
        this.transitiveGroups[key] = key
        groupNum++
      }
    }
  }
}

export function process(
  this: Solver,
  oldData: number[][],
  client: WebSocket,
): [number[][], number[][]] {
  const processQueue = () => {
    while (!this.queue.isEmpty()) {
      const pos = this.queue.shift()!
      this.doSolved(pos)
    }
  }

  // look for solvable tiles at the sides
  this.doSides()
  processQueue()

  for (let y = 0; y < this.height; y++) {
    for (let x = 0; x < this.length; x++) {
      if (!this.solved[y][x]) this.doUnsolved({ x, y })
    }
  }
  processQueue()

  this.generateGroupMap()

  const unsolvedCords = this.solved.reduce<Pos[]>((cords, row, y) => {
    row.forEach((val, x) => !val && cords.push({ y, x }))
    return cords
  }, [])

  if (unsolvedCords.length) {
    this.saveValues()

    unsolvedCords.forEach((pos) => {
      if (!this.solved[pos.y][pos.x] && !this.processed[pos.y][pos.x]) {
        this.filterByGuessing(pos)
      }
    })

    this.saveValues()
    this.doGuessing(unsolvedCords, 0)
  }

  this.sendCommands(client, oldData)

  return [
    [...Array(this.height)].map((v, i) =>
      Array.from(this.data.data.slice(i * this.length, (i + 1) * this.length)),
    ),
    this.groupMap.map((row) => [...row]),
  ]
}
