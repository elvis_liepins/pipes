import { Sides } from 'pages/pipes/types'
import { Pos } from 'types'
import { sideToPosDiff } from '..'
import { Solver } from '.'

// loop over adjacent sides of a tile
export function loopSides(
  this: Solver,
  pos: Pos,
  process: (otherPos: Pos, side: Sides, pos: Pos) => boolean,
  shouldSkip?: (otherPos: Pos, side: Sides, pos: Pos) => boolean,
): boolean {
  for (const key in Sides) {
    const side = key as Sides

    const [dx, dy] = sideToPosDiff[side]
    const x = pos.x + dx
    const y = pos.y + dy
    if (x < 0 || x === this.length) continue
    if (y < 0 || y === this.height) continue
    if (shouldSkip && shouldSkip({ x, y }, side, pos)) continue

    if (process({ x, y }, side, pos)) return true
  }
  return false
}
