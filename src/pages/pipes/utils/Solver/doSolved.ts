import { Connections, PipeShapes, PipeTiles, Sides } from 'pages/pipes/types'
import { Pos } from 'types'
import { mappedInfo, opposite } from '..'
import { Solver } from '.'

const getInfo = (symbol: number) => mappedInfo[symbol as PipeTiles]

export function doSolved(pos: Pos): boolean
export function doSolved(pos: Pos, flag: number): boolean
export function doSolved(
  pos: Pos,
  flag: number,
  connections: Connections,
  disconnects: Connections,
): boolean
export function doSolved(
  this: Solver,
  pos: Pos,
  flag = 1,
  connections?: Connections,
  disconnects?: Connections,
): boolean {
  const symbol = this.data.get(pos.y, pos.x)
  const info = getInfo(symbol)
  const connects = connections || info.connects
  const isHalf = info.type === PipeShapes.Half

  const doSolvedSide = (otherPos: Pos, side: Sides): boolean => {
    if (connects[side]) {
      const bad = this.filterRotations(
        otherPos,
        opposite[side],
        true,
        flag,
        isHalf,
      )
      if (bad) return true
    } else if (!disconnects || disconnects[side]) {
      const bad = this.filterRotations(
        otherPos,
        opposite[side],
        false,
        flag,
        isHalf,
      )
      if (bad) return true
    }
    return false
  }

  this.loopSides(pos, doSolvedSide)
  return false
}
