import Denque from 'denque'
import { Sides } from 'pages/pipes/types'
import { Pos } from 'types'
import { opposite } from '..'
import { Solver } from '.'

// this assumes that there are one or more trees, no cycles
// BFS that marks connected tiles with a key
export function makeGroupMap(this: Solver, pos: Pos, groupKey: number): number {
  const visitQueue = new Denque<{ pos: Pos; cameFrom: Sides | null }>()
  visitQueue.push({ pos, cameFrom: null })

  let count = 0
  while (!visitQueue.isEmpty()) {
    count++
    const { pos, cameFrom } = visitQueue.shift()!
    const symbol = this.data.get(pos.y, pos.x)
    const info = this.getInfo(symbol)

    this.groupMap[pos.y][pos.x] = groupKey

    const doSide = ({ x, y }: Pos, side: Sides) => {
      if (side === cameFrom || !this.solved[y][x]) return false
      if (info.connects[side]) {
        visitQueue.push({
          pos: { x, y },
          cameFrom: opposite[side],
        })
      }
      return false
    }

    this.loopSides(pos, doSide)
  }
  return count
}
