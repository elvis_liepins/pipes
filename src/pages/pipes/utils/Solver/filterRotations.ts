import { PipeShapes, PipeTiles, Sides, TileInfo } from 'pages/pipes/types'
import { Pos } from 'types'
import { mappedInfo, mappedSymbols } from '..'
import { Solver } from '.'

const getInfo = (symbol: number) => mappedInfo[symbol as PipeTiles]

// reduce legal rotations for tile depending on if it connects to a solved tile (or shouldn't connect)
export function filterRotations(
  this: Solver,
  { x, y }: Pos,
  side: Sides,
  connects: boolean,
  flag = 1,
  fromHalfTile?: boolean,
): boolean {
  if (this.solved[y][x]) return false
  const symbol = this.data.get(y, x)
  const mainInfo = getInfo(symbol)

  const isValid =
    !!fromHalfTile && mainInfo.type === PipeShapes.Half // half tiles should never connect
      ? (info: TileInfo) =>
          this.legalRotations.get(y, x, info.rotation) && !info.connects[side]
      : (info: TileInfo) =>
          this.legalRotations.get(y, x, info.rotation) &&
          connects === !!info.connects[side]

  const symbolInfo = mappedSymbols[mainInfo.type].map(getInfo)

  const legalSymbolInfo = symbolInfo.reduce((legals, info) => {
    if (isValid(info)) {
      legals.push(info)
    } else {
      this.legalRotations.set(y, x, info.rotation, 0) // remove rotation
    }
    return legals
  }, [] as TileInfo[])

  if (legalSymbolInfo.length === 1) {
    //we have a new solved tile
    this.data.set(y, x, legalSymbolInfo[0].symbol)
    this.solved[y][x] = flag
    this.queue.push({ x, y })
  } else if (legalSymbolInfo.length === 0) {
    return true // this only happens with a bad guess
  }
  return false
}
