import Denque from 'denque'
import ndarray, { NdArray } from 'ndarray'
import { Pos } from 'types'

import { PipeTiles, TileInfo } from '../../types'
import { mappedInfo } from '../tileInfo'
import { loopSides } from './loopSides'
import { filterRotations } from './filterRotations'
import {
  doSides,
  generateGroupMap,
  process as process2,
  saveValues,
} from './process'
import {
  solveBFS,
  filterByGuessing,
  setGuess,
  doGuessing,
  getLegalSymbolInfo,
  removeRotation,
  processGroups,
} from './guessing'
import { makeGroupMap } from './makeGroupMap'
import { doSolved } from './doSolved'
import {
  doIntersections,
  doUnsolved,
  processHalf,
  shouldSkipHalf,
} from './doUnsolved'

const getInfo = (symbol: number) => mappedInfo[symbol as PipeTiles]
type NumToNum = { [key: number]: number }
const rotIndexes = [0, 1, 2, 3] as const
const makeArray = (height: number, length: number) =>
  Array.from({ length: height }, () => Array.from({ length }, () => 0))

export class Solver {
  data: NdArray<Uint8Array>
  height: number
  length: number
  solved: number[][]
  savedSolved: number[][]
  legalRotations: NdArray<Uint8Array>
  savedRotations: NdArray<Uint8Array>
  groupMap: number[][] // flags of connected tiles
  savedGroupMap: number[][]
  groupSizes: NumToNum = { 0: 1 }
  transitiveGroups: NumToNum = { 0: 0 }
  processed: boolean[][]
  queue = new Denque<Pos>() // native queue with arrays is very slow

  process = process2.bind(this)
  saveValues = saveValues.bind(this)
  generateGroupMap = generateGroupMap.bind(this)

  doIntersections = doIntersections.bind(this)
  doUnsolved = doUnsolved.bind(this)
  processHalf = processHalf.bind(this)
  shouldSkipHalf = shouldSkipHalf.bind(this)

  doSides = doSides.bind(this)
  loopSides = loopSides.bind(this)
  doSolved = doSolved.bind(this)
  filterRotations = filterRotations.bind(this)

  solveBFS = solveBFS.bind(this)
  getLegalSymbolInfo = getLegalSymbolInfo.bind(this)
  filterByGuessing = filterByGuessing.bind(this)
  doGuessing = doGuessing.bind(this)
  setGuess = setGuess.bind(this)
  removeRotation = removeRotation.bind(this)
  processGroups = processGroups.bind(this)

  makeGroupMap = makeGroupMap.bind(this)
  getInfo = (symbol: number) => mappedInfo[symbol as PipeTiles]

  constructor(oldData: number[][], height: number, length: number) {
    this.data = ndarray(Uint8Array.from(oldData.flat()), [height, length])
    this.height = height
    this.length = length
    this.legalRotations = ndarray(new Uint8Array(height * length * 4).fill(1), [
      height,
      length,
      4,
    ])
    this.savedRotations = ndarray(new Uint8Array(height * length * 4).fill(1), [
      height,
      length,
      4,
    ])
    this.solved = makeArray(height, length)
    this.savedSolved = makeArray(height, length)
    this.groupMap = makeArray(height, length)
    this.savedGroupMap = makeArray(height, length)
    this.processed = Array.from({ length: height }, () =>
      Array.from({ length }, () => false),
    )
    this.queue = new Denque<Pos>()
  }

  processAsync = (oldData: number[][], client: WebSocket) => {
    return new Promise<[number[][], number[][]]>((resolve) => {
      resolve(this.process(oldData, client))
    })
  }

  sendCommands = (client: WebSocket, oldData: number[][]) => {
    const commands: string[] = []

    const generateCommand = (good: number, y: number, x: number) => {
      if (!good) return
      let symbol = oldData[y][x]
      while (!(symbol === this.data.get(y, x))) {
        commands.push(`rotate ${x} ${y}`)
        symbol = getInfo(symbol).next
      }
    }

    this.solved.forEach((row, y) =>
      row.forEach((good, x) => generateCommand(good, y, x)),
    )

    const maxMessages = 100000
    const comLen = commands.length
    for (let i = 0; i < Math.ceil(comLen / maxMessages); i++) {
      setTimeout(() => {
        client.send(commands.splice(0, maxMessages).join('\n'))
      }, 1)
    }
  }

  getRotationNumber = (pos: Pos) => {
    const rots = this.legalRotations.pick(pos.y, pos.x)
    return rotIndexes.reduce(
      (sum, index) => (sum += rots.get(index)),
      0 as number,
    )
  }

  resetGroupInfo = (
    pos: Pos,
    oldTransGroups: NumToNum,
    oldGroupSizes: NumToNum,
  ): void => {
    this.groupMap[pos.y][pos.x] = 0
    this.transitiveGroups = { ...oldTransGroups }
    this.groupSizes = { ...oldGroupSizes }
  }

  hasCycle(pos: Pos, { x, y }: Pos): boolean {
    const group1Key = this.transitiveGroups[this.groupMap[pos.y][pos.x]]
    const group2Key = this.transitiveGroups[this.groupMap[y][x]]
    if (group1Key === group2Key) {
      return true // has a cycle
    }
    return false
  }

  mergeGroups = (pos: Pos, { x, y }: Pos) => {
    const key1 = this.transitiveGroups[this.groupMap[pos.y][pos.x]]
    const key2 = this.transitiveGroups[this.groupMap[y][x]]

    if (key1 === 0) {
      this.groupMap[pos.y][pos.x] = key2
      this.groupSizes[key2] += 1
    } else {
      this.groupSizes[key2] += this.groupSizes[key1]

      //set all keys pointing from key1 to key2
      Object.values(this.transitiveGroups).forEach((transKey, index) => {
        if (transKey === key1) {
          this.transitiveGroups[index] = key2
        }
      })
    }
  }

  setSolvedRotation = (pos: Pos, info: TileInfo) => {
    const rots = this.legalRotations.pick(pos.y, pos.x)
    rotIndexes.forEach((index) =>
      rots.set(index, info.rotation === index ? 1 : 0),
    )
  }

  resetRotations = (pos: Pos) => {
    rotIndexes.forEach((index) =>
      this.legalRotations.set(
        pos.y,
        pos.x,
        index,
        this.savedRotations.get(pos.y, pos.x, index),
      ),
    )
  }

  reset = (
    pos: Pos,
    oldTransGroups: NumToNum,
    oldGroupSizes: NumToNum,
    flags: [number, number],
  ) => {
    this.solved[pos.y][pos.x] = 0
    this.resetGroupInfo(pos, oldTransGroups, oldGroupSizes)
    this.resetBFS(pos, flags)
  }

  resetBFS = (pos: Pos, flags: [number, number]) => {
    const visitQueue = new Denque<Pos>()
    const checkFlags = [...flags, 0]

    this.resetRotations(pos)
    this.groupMap[pos.y][pos.x] = this.savedGroupMap[pos.y][pos.x]
    this.solved[pos.y][pos.x] = this.savedSolved[pos.y][pos.x]

    const doSide = (pos: Pos): boolean => {
      if (checkFlags.includes(this.solved[pos.y][pos.x])) {
        if (this.solved[pos.y][pos.x] !== 0) {
          visitQueue.push(pos)
        }
        this.resetRotations(pos)
        this.groupMap[pos.y][pos.x] = this.savedGroupMap[pos.y][pos.x]
        this.solved[pos.y][pos.x] = this.savedSolved[pos.y][pos.x]
      }
      return false
    }

    visitQueue.push(pos)

    while (!visitQueue.isEmpty()) {
      const pos = visitQueue.shift()!
      this.loopSides(pos, doSide)
    }
  }
}
