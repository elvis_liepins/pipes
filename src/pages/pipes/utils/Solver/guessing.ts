import { Sides, TileInfo } from 'pages/pipes/types'
import { mappedSymbols, opposite } from '..'
import { Solver } from '.'
import { Pos } from 'types'
import Denque from 'denque'
import { NdArray } from 'ndarray'

const rotIndexes = [0, 1, 2, 3] as const

export function setGuess(
  this: Solver,
  pos: Pos,
  info: TileInfo,
  queue: Denque<Pos>,
  key: number,
) {
  queue.clear()
  queue.push(pos)
  this.data.set(pos.y, pos.x, info.symbol)
  this.solved[pos.y][pos.x] = key
}

export function solveBFS(
  this: Solver,
  queue: Denque<Pos>,
  keys: [number, number],
): boolean {
  while (!queue.isEmpty()) {
    const pos = queue.shift()!
    const info = this.getInfo(this.data.get(pos.y, pos.x))
    // this.data.set(pos.y, pos.x, info.symbol)

    if (this.doSolved(pos, keys[0])) return true
    if (this.processGroups(pos, info, keys)) return true
  }
  return false
}

export function getLegalSymbolInfo(this: Solver, pos: Pos) {
  const isValid = (info: TileInfo) =>
    this.legalRotations.get(pos.y, pos.x, info.rotation) === 1

  const symbol = this.data.get(pos.y, pos.x)
  const symbols = mappedSymbols[this.getInfo(symbol).type]
  return symbols.map(this.getInfo).filter(isValid)
}

export function filterByGuessing(this: Solver, pos: Pos) {
  const legalSymbolInfo = this.getLegalSymbolInfo(pos)
  const oldTransGroups = { ...this.transitiveGroups }
  const oldGroupSizes = { ...this.groupSizes }

  const flags: [number, number] = [2, 3]
  let goodRotations = 0

  for (const info of legalSymbolInfo) {
    this.setGuess(pos, info, this.queue, flags[0])

    if (this.solveBFS(this.queue, flags)) {
      this.removeRotation(pos, info)
    } else {
      goodRotations++
    }

    this.reset(pos, oldTransGroups, oldGroupSizes, flags)
  }

  if (goodRotations !== 1) return

  const goodRotation = legalSymbolInfo.find(
    (info) => !!this.legalRotations.get(pos.y, pos.x, info.rotation),
  )!

  this.setGuess(pos, goodRotation, this.queue, flags[0])
  this.solveBFS(this.queue, [flags[0], 1])
  this.queue.clear()
}

export function doGuessing(
  this: Solver,
  unsolvedList: Pos[],
  index: number,
): boolean {
  if (index === unsolvedList.length)
    return (
      this.groupSizes[this.transitiveGroups[1]] !== this.height * this.length
    )
  const pos = unsolvedList[index]
  if (this.solved[pos.y][pos.x]) return this.doGuessing(unsolvedList, index + 1)

  // console.log(index)

  const legalSymbolInfo = this.getLegalSymbolInfo(pos)

  const oldTransGroups = { ...this.transitiveGroups }
  const oldGroupSizes = { ...this.groupSizes }

  const flags: [number, number] = [index * 2 + 4, index * 2 + 5]

  for (const info of legalSymbolInfo) {
    this.setGuess(pos, info, this.queue, flags[0])

    const wasBad = this.solveBFS(this.queue, flags)
    if (!wasBad && !this.doGuessing(unsolvedList, index + 1)) {
      return false
    }
    this.reset(pos, oldTransGroups, oldGroupSizes, flags)
  }
  this.queue.clear()
  return true
}

export function removeRotation(
  this: Solver,
  pos: Pos,
  info: TileInfo,
  oldRotations?: NdArray<Uint8Array>,
) {
  rotIndexes.forEach((index) => {
    if (info.rotation === index) {
      this.legalRotations.set(pos.y, pos.x, index, 0)
      this.savedRotations.set(pos.y, pos.x, index, 0)
      oldRotations?.set(pos.y, pos.x, index, 0)
    }
  })
}

export function processGroups(
  this: Solver,
  pos: Pos,
  info: TileInfo,
  flags: [number, number],
): boolean {
  this.data.set(pos.y, pos.x, info.symbol)
  this.setSolvedRotation(pos, info)
  this.solved[pos.y][pos.x] = flags[1]
  this.processed[pos.y][pos.x] = true

  const doSide = (otherPos: Pos, side: Sides): boolean => {
    if (
      this.solved[otherPos.y][otherPos.x] === 0 ||
      this.solved[otherPos.y][otherPos.x] === flags[0]
    ) {
      return false
    }
    const other = this.getInfo(this.data.get(otherPos.y, otherPos.x))
    if (info.connects[side] !== other.connects[opposite[side]]) {
      return true
    }

    if (!info.connects[side]) return false // check groups only if connected
    if (this.hasCycle(pos, otherPos)) {
      return true
    }
    this.mergeGroups(pos, otherPos)

    return false
  }

  if (this.loopSides(pos, doSide)) return true
  //if not yed connected, make a new group
  if (this.transitiveGroups[this.groupMap[pos.y][pos.x]] === 0) {
    const key = Object.values(this.groupSizes).length
    this.groupSizes[key] = 1
    this.transitiveGroups[key] = key
    this.groupMap[pos.y][pos.x] = key
  }

  return false
}
