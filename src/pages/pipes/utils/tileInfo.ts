import { PipeShapes, PipeTiles, TileInfo } from '../types'

const up = true
const right = true
const down = true
const left = true

export const mappedInfo: { [key in PipeTiles]: TileInfo } = {
  [PipeTiles.TUp]: {
    symbol: PipeTiles.TUp,
    type: PipeShapes.T,
    rotation: 0,
    next: PipeTiles.TRight,
    connects: { left, up, right },
  },
  [PipeTiles.TRight]: {
    symbol: PipeTiles.TRight,
    type: PipeShapes.T,
    rotation: 1,
    next: PipeTiles.TDown,
    connects: { up, right, down },
  },
  [PipeTiles.TDown]: {
    symbol: PipeTiles.TDown,
    type: PipeShapes.T,
    rotation: 2,
    next: PipeTiles.TLeft,
    connects: { right, down, left },
  },
  [PipeTiles.TLeft]: {
    symbol: PipeTiles.TLeft,
    type: PipeShapes.T,
    rotation: 3,
    next: PipeTiles.TUp,
    connects: { left, up, down },
  },
  [PipeTiles.HalfUp]: {
    symbol: PipeTiles.HalfUp,
    type: PipeShapes.Half,
    rotation: 0,
    next: PipeTiles.HalfRight,
    connects: { up },
  },
  [PipeTiles.HalfRight]: {
    symbol: PipeTiles.HalfRight,
    type: PipeShapes.Half,
    rotation: 1,
    next: PipeTiles.HalfDown,
    connects: { right },
  },
  [PipeTiles.HalfDown]: {
    symbol: PipeTiles.HalfDown,
    type: PipeShapes.Half,
    rotation: 2,
    next: PipeTiles.HalfLeft,
    connects: { down },
  },
  [PipeTiles.HalfLeft]: {
    symbol: PipeTiles.HalfLeft,
    type: PipeShapes.Half,
    rotation: 3,
    next: PipeTiles.HalfUp,
    connects: { left },
  },
  [PipeTiles.Vertical]: {
    symbol: PipeTiles.Vertical,
    type: PipeShapes.Line,
    rotation: 0,
    next: PipeTiles.Horizontal,
    connects: { up, down },
  },
  [PipeTiles.Horizontal]: {
    symbol: PipeTiles.Horizontal,
    type: PipeShapes.Line,
    rotation: 1,
    next: PipeTiles.Vertical,
    connects: { left, right },
  },
  [PipeTiles.Cross]: {
    symbol: PipeTiles.Cross,
    type: PipeShapes.Cross,
    rotation: 0,
    next: PipeTiles.Cross,
    connects: { up, right, down, left },
  },
  [PipeTiles.LUp]: {
    symbol: PipeTiles.LUp,
    type: PipeShapes.L,
    rotation: 0,
    next: PipeTiles.LRight,
    connects: { up, right },
  },
  [PipeTiles.LRight]: {
    symbol: PipeTiles.LRight,
    type: PipeShapes.L,
    rotation: 1,
    next: PipeTiles.LDown,
    connects: { right, down },
  },
  [PipeTiles.LDown]: {
    symbol: PipeTiles.LDown,
    type: PipeShapes.L,
    rotation: 2,
    next: PipeTiles.LLeft,
    connects: { down, left },
  },
  [PipeTiles.LLeft]: {
    symbol: PipeTiles.LLeft,
    type: PipeShapes.L,
    rotation: 3,
    next: PipeTiles.LUp,
    connects: { left, up },
  },
} as const
