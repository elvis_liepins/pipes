import { CrossIcon, HalfIcon, IconProps, LIcon, LineIcon, TIcon } from 'icons'
import { LegalRotations, PipeShapes, PipeTiles, Sides } from '../types'
import { Solver } from './Solver'
import { makeDataMap } from './parseData'
import { mappedInfo } from './tileInfo'

// export { mappedInfo, makeDataMap, solve }
export { mappedInfo, makeDataMap, Solver }

export const defConnections = {
  up: true,
  right: true,
  down: true,
  left: true,
}

export const defRotations: LegalRotations = {
  0: true,
  1: true,
  2: true,
  3: true,
}

export const opposite = {
  [Sides.up]: Sides.down,
  [Sides.right]: Sides.left,
  [Sides.down]: Sides.up,
  [Sides.left]: Sides.right,
} as const

type PosDiff = -1 | 0 | 1
// export const sideToPosDiff: { [key: string]: readonly [PosDiff, PosDiff] } = {
export const sideToPosDiff: { [key in Sides]: readonly [PosDiff, PosDiff] } = {
  [Sides.up]: [0, -1],
  [Sides.right]: [1, 0],
  [Sides.down]: [0, 1],
  [Sides.left]: [-1, 0],
} as const

export const mappedSymbols: { [key in PipeShapes]: readonly PipeTiles[] } = {
  [PipeShapes.T]: [
    PipeTiles.TUp,
    PipeTiles.TRight,
    PipeTiles.TDown,
    PipeTiles.TLeft,
  ],
  [PipeShapes.Half]: [
    PipeTiles.HalfUp,
    PipeTiles.HalfRight,
    PipeTiles.HalfDown,
    PipeTiles.HalfLeft,
  ],
  [PipeShapes.Line]: [PipeTiles.Vertical, PipeTiles.Horizontal],
  [PipeShapes.Cross]: [PipeTiles.Cross],
  [PipeShapes.L]: [
    PipeTiles.LUp,
    PipeTiles.LRight,
    PipeTiles.LDown,
    PipeTiles.LLeft,
  ],
} as const

export const mappedIcons: {
  [key in PipeShapes]: (props: IconProps) => JSX.Element
} = {
  [PipeShapes.T]: TIcon,
  [PipeShapes.Half]: HalfIcon,
  [PipeShapes.Line]: LineIcon,
  [PipeShapes.Cross]: CrossIcon,
  [PipeShapes.L]: LIcon,
} as const

export const symbolToEnum = {
  '┻': PipeTiles.TUp,
  '┣': PipeTiles.TRight,
  '┳': PipeTiles.TDown,
  '┫': PipeTiles.TLeft,
  '╹': PipeTiles.HalfUp,
  '╺': PipeTiles.HalfRight,
  '╻': PipeTiles.HalfDown,
  '╸': PipeTiles.HalfLeft,
  '━': PipeTiles.Horizontal,
  '┃': PipeTiles.Vertical,
  '╋': PipeTiles.Cross,
  '┗': PipeTiles.LUp,
  '┏': PipeTiles.LRight,
  '┓': PipeTiles.LDown,
  '┛': PipeTiles.LLeft,
} as const
