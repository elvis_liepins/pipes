import { symbolToEnum } from '.'
import { PipeSymbols } from '../types'

export function makeDataMap(map: string): number[][] {
  let first = map.indexOf('\n') + 1
  const length = map.indexOf('\n', first) - first

  const data: number[][] = []
  while (first < map.length) {
    const row = Array(length)

    for (let i = 0; i < length; i++) {
      row[i] = symbolToEnum[map[first + i] as PipeSymbols]
    }

    data.push(row)
    first += length + 1 // for \n
  }

  return data
}

export const makeDataMapPromise = (map: string) =>
  new Promise<number[][]>((resolve) => resolve(makeDataMap(map)))
