import React from 'react'

export type Scene = 'menu' | 'game'
export type Level = 1 | 2 | 3 | 4 | 5 | 6

const context = {
  scene: 'menu' as Scene,
  level: 1 as Level,
  setScene: function (scene: Scene, level: Level, loading?: boolean) {
    this.scene = scene
    this.level = level
  },
}

export const SceneContext = React.createContext(context)
