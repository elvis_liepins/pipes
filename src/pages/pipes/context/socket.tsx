import React from 'react'

const url = 'wss://hometask.eg1236.com/game-pipes/'

const client = new WebSocket(url)

client.onopen = function (): void {
  console.log('connected')
}

const send = (command: string) => {
  if (client.OPEN) {
    client.send(command)
  }
}

const context = { client, send }

export const SocketContext = React.createContext(context)

interface props {
  children: React.ReactNode
}

export const WebSocketProvider = ({ children }: props) => (
  <SocketContext.Provider value={context}>{children}</SocketContext.Provider>
)
