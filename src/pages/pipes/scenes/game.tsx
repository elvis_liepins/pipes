import React from 'react'

import { TileMap } from 'components/TileMap'
import { Tile } from '../components/Tile'
import { Controls } from '../components/Controls'

export const Game = () => (
  <div>
    <TileMap tileComponent={Tile} />
    <Controls />
  </div>
)
