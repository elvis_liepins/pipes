import React from 'react'
import { createUseStyles } from 'react-jss'
import { LevelButtons } from '../components/LevelButtons'

const useStyles = createUseStyles({
  center: {
    '& > p': {
      fontSize: '0.4em',
    },
    display: 'grid',
    placeItems: 'center',
    fontSize: '3em',
    alignContent: 'center',
    textAlign: 'center',
  },
})

export const Menu = () => {
  const classes = useStyles()

  return (
    <div className={classes.center}>
      <h1>Pipes</h1>
      <p>
        Rotate the tiles on the map to make all pipes connected in a single
        group, with no loops and no dangling pipes
      </p>
      <h6>Choose level:</h6>
      <LevelButtons />
    </div>
  )
}
