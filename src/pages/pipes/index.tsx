import React, { useContext, useState } from 'react'
import { useDispatch } from 'react-redux'
import Snackbar from '@material-ui/core/Snackbar'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import Alert from '@material-ui/lab/Alert'

import { Game } from './scenes/game'
import { Menu } from './scenes/menu'
import { setMap } from 'reducers/map'
import { SocketContext, WebSocketProvider } from 'pages/pipes/context/socket'
import { Level, Scene, SceneContext } from './context/scene'
import { makeDataMapPromise } from './utils/parseData'

const PipesPage = () => {
  const context = useContext(SocketContext)
  const dispatch = useDispatch()
  const [snackbarText, setSnackbar] = useState('')
  const [loading, setLoading] = useState(false)
  const [scene, setScene] = useState<Scene>('menu')
  const [level, setLevel] = useState<Level>(0 as Level)

  const handleSceneChange = (scene: Scene, level: Level, loading?: boolean) => {
    setScene(scene)
    setLevel(level)
    if (scene === 'game') {
      setLoading(true)
    }
    if (loading !== undefined) {
      setLoading(loading)
    }
  }

  context.client.onmessage = (event: MessageEvent) => {
    const { data } = event
    if (data.startsWith('map:')) {
      makeDataMapPromise(data).then((res) => {
        dispatch(setMap(res))
        setLoading(false)
      })
    } else if (data.startsWith('verify:')) {
      setSnackbar(data.slice('verify: '.length))
      console.log(data)
    }
  }

  return (
    <SceneContext.Provider
      value={{ scene, level, setScene: handleSceneChange }}
    >
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={!!snackbarText}
        autoHideDuration={3000}
        onClose={() => setSnackbar('')}
      >
        <Alert
          onClose={() => setSnackbar('')}
          severity={snackbarText.startsWith('Correct') ? 'success' : 'error'}
        >
          {snackbarText}
        </Alert>
      </Snackbar>
      <Backdrop open={loading}>
        <CircularProgress color="inherit" />
      </Backdrop>
      {scene === 'menu' ? <Menu /> : <Game />}
    </SceneContext.Provider>
  )
}

const Wrapper = () => (
  <WebSocketProvider>
    <PipesPage />
  </WebSocketProvider>
)

export default Wrapper
