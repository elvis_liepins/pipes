import React from 'react'

interface Props extends IconProps {
  children: React.ReactNode
}

const BaseIcon = ({ height = 48, children, ...rest }: Props) => (
  <svg viewBox="0 0 24 24" {...{ height, ...rest }}>
    {children}
  </svg>
)

export interface IconProps {
  height?: 72 | 60 | 48 | 36
  style?: React.CSSProperties
  className?: string
}

export const CrossIcon = (props: IconProps) => (
  <BaseIcon {...props}>
    <path d="M 12 0 V 24 M 0 12 H 24" stroke="black" strokeWidth="4px" />
  </BaseIcon>
)
export const TIcon = (props: IconProps) => (
  <BaseIcon {...props}>
    <path d="M 12 0 V 12 M 0 12 H 24" stroke="black" strokeWidth="4px" />
  </BaseIcon>
)
export const LIcon = (props: IconProps) => (
  <BaseIcon {...props}>
    <path d="M 12 0 V 12 M 10 12 H 24" stroke="black" strokeWidth="4px" />
  </BaseIcon>
)
export const HalfIcon = (props: IconProps) => (
  <BaseIcon {...props}>
    <path d="M 12 0 V 14" stroke="black" strokeWidth="4px" />
  </BaseIcon>
)
export const LineIcon = (props: IconProps) => (
  <BaseIcon {...props}>
    <path d="M 12 0 V 24" stroke="black" strokeWidth="4px" />
  </BaseIcon>
)
