import { configureStore } from '@reduxjs/toolkit'
import map from './map'

const store = configureStore({
  reducer: {
    map,
  },
})

export default store
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
