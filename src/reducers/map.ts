import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Pos } from 'types'

interface MapState {
  data: number[][]
  solved: number[][]
  height: number
  length: number
  mapNum: number // used to force complete rerender when map changes
}
const initalState: MapState = {
  data: [[]],
  solved: [[]],
  height: 0,
  length: 0,
  mapNum: 0,
}

type ChangeTilePayload = {
  cords: Pos
  info: number
}

export const mapSlice = createSlice({
  name: 'map',
  initialState: initalState,
  reducers: {
    setMap: (state, action: PayloadAction<number[][]>) => {
      const data = action.payload
      state.data = data
      state.mapNum = state.mapNum + 1
      state.height = data.length
      state.length = data[0]?.length || 0
      state.solved = Array<Array<number>>(state.height).fill(
        Array(state.length).fill(0),
      )
    },
    changeMap: (state, action: PayloadAction<[number[][], number[][]]>) => {
      const data = action.payload[0]
      const solved = action.payload[1]

      state.mapNum = state.mapNum + 1
      state.data = data
      state.solved = solved
      state.height = data.length
      state.length = data[0].length
    },
    changeTile: (state, action: PayloadAction<ChangeTilePayload>) => {
      const {
        cords: { x, y },
        info: symbol,
      } = action.payload

      state.data[y][x] = symbol
    },
  },
})

export const { setMap, changeTile, changeMap } = mapSlice.actions

export default mapSlice.reducer
