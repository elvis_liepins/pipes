import React, { memo } from 'react'
import { Pos } from 'types'
import { TileComponent } from '../types'
import { Row } from './Row'

export interface TileGridProps {
  data: number[][]
  // mapNum: number
  cornerPos: Pos
  length: number
  height: number
  tileComponent: TileComponent
}

export const TileGridComponent = ({
  data,
  cornerPos: { x, y },
  length,
  height,
  tileComponent,
}: TileGridProps) => {
  return (
    <div>
      {/* {[...Array(height)].map((row, dy) => ( */}
      {data.slice(y, y + height).map((row, dy) => (
        <Row
          key={y + dy}
          data={row}
          tileComponent={tileComponent}
          length={length}
          pos={{ x, y: y + dy }}
        />
      ))}
    </div>
  )
}

export const TileGrid = memo(
  TileGridComponent,
  (prev, next) =>
    prev.cornerPos.x === next.cornerPos.x &&
    prev.cornerPos.y === next.cornerPos.y &&
    prev.length == next.length &&
    prev.height == next.height,
)
