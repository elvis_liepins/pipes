import React from 'react'
import { createUseStyles } from 'react-jss'
import { IconButton } from '@material-ui/core'

import { LIcon } from 'icons'
import { Pos } from 'types'

const useStyles = createUseStyles({
  coordinates: {
    fontSize: '48px',
    fontFamily: '"Roboto Condensed", sans-serif',
  },
  horizontalControls: {
    gridColumn: '2 / 3',
    display: 'flex',
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  verticalControls: {
    gridRow: '2 / span 1',
    gridColumn: '3 / span 1',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'baseline',
    justifyContent: 'center',
  },
  leftIcon: {
    transform: 'rotate(45deg)',
  },
  rightIcon: {
    transform: 'rotate(-135deg)',
  },
  upIcon: {
    transform: 'rotate(135deg)',
  },
  downIcon: {
    transform: 'rotate(-45deg)',
  },
})

export interface ControlsProps {
  pos: Pos
  setPos: (pos: Pos) => void
  maxLen: number
  maxRows: number
  length: number
  height: number
}

export const Controls = ({
  pos: { x, y },
  setPos,
  maxLen,
  maxRows,
  length,
  height,
}: ControlsProps) => {
  const classes = useStyles()
  const changePos = (pos: Pos) => {
    setPos(pos)
  }

  const leftDisabled = x === 0
  const rightDisabled = x === length - maxLen
  const upDisabled = y === 0
  const downDisabled = y === height - maxRows

  return (
    <>
      <div className={classes.horizontalControls}>
        <IconButton
          disabled={leftDisabled}
          onClick={() => changePos({ x: x - 1, y })}
        >
          <LIcon
            className={classes.leftIcon}
            style={{ opacity: leftDisabled ? '0.5' : '1' }}
          />
        </IconButton>
        <IconButton
          disabled={rightDisabled}
          onClick={() => changePos({ x: x + 1, y })}
        >
          <LIcon
            className={classes.rightIcon}
            style={{ opacity: rightDisabled ? '0.5' : '1' }}
          />
        </IconButton>
      </div>
      <div className={classes.verticalControls}>
        <IconButton
          disabled={upDisabled}
          onClick={() => changePos({ x, y: y - 1 })}
        >
          <LIcon
            className={classes.upIcon}
            style={{ opacity: upDisabled ? '0.5' : '1' }}
          />
        </IconButton>
        <IconButton
          disabled={downDisabled}
          onClick={() => changePos({ x, y: y + 1 })}
        >
          <LIcon
            className={classes.downIcon}
            style={{ opacity: downDisabled ? '0.5' : '1' }}
          />
        </IconButton>
      </div>
    </>
  )
}
