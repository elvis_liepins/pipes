import React from 'react'
import { createUseStyles } from 'react-jss'
import { Pos } from 'types'
import { TileComponent } from '../types'

const useStyles = createUseStyles({
  rowContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
})

export interface RowProps {
  data: number[]
  pos: Pos
  length: number
  tileComponent: TileComponent
}

// export const Row = ({ data, pos, length, tileComponent: Tile }: RowProps) => {
export const Row = ({ data, pos, length, tileComponent: Tile }: RowProps) => {
  const classes = useStyles()
  return (
    <div className={classes.rowContainer}>
      {data.slice(pos.x, pos.x + length).map((data, dx) => {
        const x = pos.x + dx
        return (
          <Tile key={`${x}:${pos.y}`} data={data} cords={{ x, y: pos.y }} />
        )
      })}
    </div>
  )
}
