import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import { RootState } from 'reducers/store'
import { createUseStyles } from 'react-jss'

import { Controls } from './components/Controls'
import { TileGrid } from './components/TileGrid'
import { TileComponent } from './types'

const useStyles = createUseStyles({
  container: {
    display: 'grid',
    gridTemplateColumns: '1fr auto 1fr',
    gridTemplateRows: 'repeat(2, auto)',
    gap: '24px',
  },
  tileGridContainer: {
    gridRow: '2 / span 1',
    gridColumn: '2 / span 1',
  },
})

export interface TileMapProps {
  tileComponent: TileComponent
}

const maxSize = 15
export const TileMap = ({ tileComponent }: TileMapProps) => {
  const { mapNum, data, height, length } = useSelector(
    (state: RootState) => state.map,
  )

  return mapNum ? (
    <Layout key={mapNum} {...{ data, height, length, tileComponent }} />
  ) : null
}

interface LayoutProps {
  data: number[][]
  height: number
  length: number
  tileComponent: TileComponent
}

const Layout = ({ height, length, tileComponent, data }: LayoutProps) => {
  const [cornerPos, setPos] = useState({ x: 0, y: 0 })
  const classes = useStyles()

  const maxRows = maxSize > height ? height : maxSize
  const maxLen = maxSize > length ? length : maxSize

  return (
    <div className={classes.container}>
      <Controls
        pos={cornerPos}
        {...{ setPos, maxLen, maxRows, length, height }}
      />
      <div className={classes.tileGridContainer}>
        <TileGrid
          data={data}
          tileComponent={tileComponent}
          height={maxRows}
          length={maxLen}
          cornerPos={cornerPos}
        />
      </div>
    </div>
  )
}
