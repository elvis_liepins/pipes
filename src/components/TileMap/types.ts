import { Pos } from 'types'

export interface TileProps {
  data: number
  cords: Pos
}

export type TileComponent = (props: TileProps) => JSX.Element
